//Waratchapon Ponpiya 61160044 sec2
import java.util.Scanner;

public class OX {
     public static String[][] tableString =   { {"-","-","-"},
                                                   {"-","-","-"},
                                                   {"-","-","-"} };
     public static String player = "X";
     public static int row , col;
     public static int countCycle = 0;
     
    public static void main(String[] args){ 
        showWelcome();
        while(true){
            showTable();
            showTurn();
            input();
            if(checkWin()){
                showTable();
                showWin();
                break;
            }
            if(countCycle==9 && checkWin()== false ){
                showTable();
                showDraw();
                break;
            }
            switchPlayer();
        }
        showBye();
    }
    
    public static void showWelcome(){
        System.out.println("Welcome to OX Game");
    }
    
    public static void showTable(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
              System.out.print(tableString[i][j]+" ");
            }
            System.out.println();
        }
    }
    
     public static void switchPlayer(){
        if(player == "O"){
            player = "X";
        }else if(player == "X"){
            player = "O";
        }
    }
    
    public static void showTurn(){
        System.out.println("Turn "+player);
         countCycle++;
    }
    
    public static void input(){
        Scanner kb = new Scanner(System.in);
        while(row<1 || row>3 || col <1 || col >3 ||
                !(tableString[row-1][col-1].equals("-")) ){
            System.out.print("Please input row, col: ");
            row = kb.nextInt();
            col = kb.nextInt();
            if(row<1 || row>3 || col <1 || col >3 ){
              System.out.println("Incorrect information!!");
            }else if(!(tableString[row-1][col-1].equals("-"))){
                System.out.println("Repeat variable!!");
            }
            }
        
            row -= 1; col -= 1;
            tableString[row][col] = player;
        }
    
    
    public static boolean checkWin(){
        if(checkWinrow()){
            return true;
        }else if (checkWincol()){
            return true;
        }else if(checkWincrosswise()){
            return true;
        }
        return false;
    }
    
    public static boolean checkWinrow() {
        if(tableString[0][0] == player && tableString[0][1] == player 
                && tableString[0][2] == player){
            return true;
        }else if (tableString[1][0] == player && tableString[1][1] == player
                && tableString[1][2] == player){
            return true;
        }else if(tableString[2][0] == player && tableString[2][1] == player
                && tableString[2][2] == player){
            return true;
        }
        return false;
    }
    
    public static boolean checkWincol() {
        if(tableString[0][0] == player && tableString[1][0] == player 
                && tableString[2][0] == player){
            return true;
        }else if (tableString[0][1] == player && tableString[1][1] == player
                && tableString[2][1] == player){
            return true;
        }else if(tableString[0][2] == player && tableString[1][2] == player
                && tableString[2][2] == player){
            return true;
        }
        return false;
    }
    
    public static boolean checkWincrosswise(){
       if(tableString[0][0] == player && tableString[1][1] == player 
                && tableString[2][2] == player){
            return true;
        }else if (tableString[0][2] == player && tableString[1][1] == player
                && tableString[2][0] == player){
            return true;
        }
        return false;
    }
    
    public static void showDraw(){
        System.out.println("Draw in this game.");
    }
    
    public static void showWin(){
        System.out.println(player+" Win");
    }
    
    public static void showBye(){
        System.out.println("Bye bye ....");
    }
    
}